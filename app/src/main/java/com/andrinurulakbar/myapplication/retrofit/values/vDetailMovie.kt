package com.andrinurulakbar.myapplication.retrofit.value

import com.andrinurulakbar.myapplication.retrofit.models.mGenres


data class vDetailMovie(
    val genres: List<mGenres>,
    val original_title: String,
    val overview: String,
    val poster_path: String,
    val vote_average: String
)
