package com.andrinurulakbar.myapplication.retrofit

import com.andrinurulakbar.myapplication.retrofit.value.vDetailMovie
import com.andrinurulakbar.myapplication.retrofit.value.vResults
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ServiceApi {
    @GET("3/movie/{id}")
    suspend fun getNowPlaying(@Path("id") id: String,
                              @Query("api_key") api_key: String,
                              @Query("language") language: String,
                              @Query("page") page: Int
    ) : Response<vResults>

    @GET("3/movie/{id}")
    suspend fun getDetail(@Path("id") id: Int,
                          @Query("api_key") api_key: String,
                          @Query("language") language: String
    ) : Response<vDetailMovie>
}