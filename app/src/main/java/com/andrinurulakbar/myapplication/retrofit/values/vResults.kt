package com.andrinurulakbar.myapplication.retrofit.value

import com.andrinurulakbar.myapplication.retrofit.models.mDates
import com.andrinurulakbar.myapplication.retrofit.models.mResults


data class vResults(
    val dates: mDates,
    val page: Integer,
    val results: List<mResults>,
    val total_pages: Integer,
    val total_results: Integer
)
