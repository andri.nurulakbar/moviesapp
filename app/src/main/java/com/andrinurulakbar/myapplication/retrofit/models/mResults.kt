package com.andrinurulakbar.myapplication.retrofit.models

data class mResults(
    val dates: Boolean,
    val backdrop_path: String,
    val genre_ids: List<Integer>,
    val id: Integer,
    val original_language: String,
    val original_title: String,
    val overview: String,
    val popularity: String,
    val poster_path: String,
    val release_date: String,
    val title: String,
    val video: Boolean,
    val vote_average: Double,
    val vote_count: Integer
)
