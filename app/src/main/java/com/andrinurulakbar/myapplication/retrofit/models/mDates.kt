package com.andrinurulakbar.myapplication.retrofit.models

data class mDates (
    val maximum: String,
    val minimum: String
)