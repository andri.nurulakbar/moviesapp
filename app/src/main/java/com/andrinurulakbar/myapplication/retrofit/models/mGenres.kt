package com.andrinurulakbar.myapplication.retrofit.models

data class mGenres(
    val id: Int,
    val name: String
)
