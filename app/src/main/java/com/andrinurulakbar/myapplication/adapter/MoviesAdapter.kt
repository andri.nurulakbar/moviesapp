package com.andrinurulakbar.myapplication.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andrinurulakbar.myapplication.R
import com.andrinurulakbar.myapplication.activity.DetailMovie
import com.andrinurulakbar.myapplication.helper.MovieHelper
import com.andrinurulakbar.myapplication.retrofit.models.mResults
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie.view.*

class MoviesAdapter(private val movie: List<mResults>, val context : Context, val activity : Activity) : RecyclerView.Adapter<MoviesAdapter.MovieHolder>()  {


    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): MovieHolder {
        return MovieHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_movie, viewGroup, false))
    }

    override fun getItemCount(): Int = movie.size

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
//        holder.bindMovie(movie[position])
        holder.itemView.txtJudul.text = movie.get(position).original_title
        holder.itemView.txtRating.text = movie.get(position).vote_average.toString()
        println("trace image "+ MovieHelper.urlImage+movie.get(position).poster_path)
        Picasso.get().load(MovieHelper.urlImage+movie.get(position).poster_path).into(holder.itemView.imagePoster)
        holder.itemView.card_ganjil.setOnClickListener {
            val intent = Intent(context.applicationContext, DetailMovie::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("movieId",movie.get(position).id.toString())
            context.startActivity(intent)
        }



    }

    class MovieHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}