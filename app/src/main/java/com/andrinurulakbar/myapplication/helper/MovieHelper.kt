package com.andrinurulakbar.myapplication.helper

class MovieHelper {
    companion object {
        const val serverUrlApi = "https://api.themoviedb.org/"
        const val apiKey = "11d9b336e7e28ac6d625134abfdadd4e"
        const val language = "en-US"
        const val urlImage = "http://image.tmdb.org/t/p/w500"
    }
}