package com.andrinurulakbar.myapplication.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.andrinurulakbar.myapplication.R
import com.andrinurulakbar.myapplication.helper.MovieHelper
import com.andrinurulakbar.myapplication.retrofit.Koneksi
import com.andrinurulakbar.myapplication.retrofit.ServiceApi
import com.andrinurulakbar.myapplication.retrofit.models.mGenres
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailMovie : AppCompatActivity() {
    var genres: List<mGenres> = emptyList()
    var idMovie: Int = 0
    var textGenre: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)
        supportActionBar!!.hide()

        val intents = intent
        idMovie    = intents.getStringExtra("movieId")!!.toInt()

        println("trace param id "+idMovie)

        val retro = Koneksi().getInstance().create(ServiceApi::class.java)
        lifecycleScope.launchWhenCreated {
            try {
                val response = retro.getDetail(idMovie, MovieHelper.apiKey, MovieHelper.language)
                if (response.isSuccessful()) {
                    //your code for handaling success response

                    txtJudul.text = response.body()!!.original_title
                    txtOverview.text = response.body()!!.overview
                    txtRating.text = response.body()!!.vote_average
                    Picasso.get().load(MovieHelper.urlImage+response.body()!!.poster_path).into(imagePoster)
                    genres = response.body()!!.genres
                    for (i in 0..genres.size){
                        println("trace genre "+genres.get(i).name)
                        if (i == genres.size-1){
                            txtGenre.text = txtGenre.text.toString()+genres.get(i).name
                        } else {
                            txtGenre.text = txtGenre.text.toString()+genres.get(i).name+", "
                        }
                    }
//                    println("trace teks genre "+textGenre)
//                    txtGenre.text = textGenre.toString()
//                    resultMovie = response.body()?.results ?: resultMovie
//                    val moviesAdapter = MoviesAdapter(resultMovie, requireContext().applicationContext, requireActivity())
//                    root.recycler.apply {
//                        layoutManager = GridLayoutManager(context.getApplicationContext(), 2)
//                        adapter = moviesAdapter
//                    }
                    println("trace size genre "+genres.size)
                } else {
                    Toast.makeText(
                        applicationContext,
                        response.errorBody().toString(),
                        Toast.LENGTH_LONG
                    ).show()
                    println("trace error "+response.errorBody()!!.string())
                }
            }catch (Ex:Exception){
                Log.e("Error",Ex.localizedMessage)
                println("trace error "+Ex.localizedMessage)
            }
        }
    }
}