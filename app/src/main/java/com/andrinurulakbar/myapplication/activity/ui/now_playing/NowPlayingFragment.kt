package com.andrinurulakbar.myapplication.activity.ui.now_playing

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.andrinurulakbar.myapplication.R
import com.andrinurulakbar.myapplication.adapter.MoviesAdapter
import com.andrinurulakbar.myapplication.helper.MovieHelper
import com.andrinurulakbar.myapplication.retrofit.Koneksi
import com.andrinurulakbar.myapplication.retrofit.ServiceApi
import com.andrinurulakbar.myapplication.retrofit.models.mResults
import kotlinx.android.synthetic.main.fragment_now_playing.view.*

class NowPlayingFragment : Fragment() {

    var resultMovie: List<mResults> = emptyList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

//        kehadiranViewModel =
//                new ViewModelProvider(this).get(KehadiranViewModel.class);
        val root: View = inflater.inflate(R.layout.fragment_now_playing, container, false)
        val retro = Koneksi().getInstance().create(ServiceApi::class.java)
        lifecycleScope.launchWhenCreated {
            try {
                val response = retro.getNowPlaying("now_playing", MovieHelper.apiKey, MovieHelper.language, 1)
                if (response.isSuccessful()) {
                    //your code for handaling success response

                    resultMovie = response.body()?.results ?: resultMovie
                    val moviesAdapter = MoviesAdapter(resultMovie, requireContext().applicationContext, requireActivity())
                    root.recycler.apply {
                        layoutManager = GridLayoutManager(context.getApplicationContext(), 2)
                        adapter = moviesAdapter
                    }
                    println("trace size "+resultMovie.size)
                } else {
                    Toast.makeText(
                        requireActivity(),
                        response.errorBody().toString(),
                        Toast.LENGTH_LONG
                    ).show()
                    println("trace error "+response.errorBody()!!.string())
                }
            }catch (Ex:Exception){
                Log.e("Error",Ex.localizedMessage)
                println("trace error "+Ex.localizedMessage)
            }
        }
        return root
    }
}